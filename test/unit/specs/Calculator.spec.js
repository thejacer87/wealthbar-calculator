import Vue from 'vue'
import { mount } from '@vue/test-utils'
import Calculator from '@/components/Calculator'

describe('Calculator', () => {
  it('should render correct contents', () => {
    const Constructor = Vue.extend(Calculator)
    const vm = new Constructor().$mount()
    expect(vm.$el.querySelector('#calculator h1').textContent)
      .toEqual('WealthBar TFSA v RRSP')
  })

  const wrapper = mount(Calculator)
  const button = wrapper.find('button')

  it('should calculate TFSA', () => {
    // setup test values
    wrapper.vm.userData.marginalTaxRate = 38.29
    wrapper.vm.userData.retirementTaxRate = 20.64
    wrapper.vm.userData.deposit = 1000
    wrapper.vm.userData.years = 30
    wrapper.vm.userData.roi = 6.5
    wrapper.vm.userData.inflation = 2.5

    // calculate
    button.trigger('click')
    expect(wrapper.vm.tfsaData.totalDeposit.toFixed(2)).toBe("1000.00")
    expect(wrapper.vm.tfsaData.futureValue.toFixed(2)).toBe("3153.35")
    expect(wrapper.vm.tfsaData.futureTaxPaid.toFixed(2)).toBe("0.00")
    expect(wrapper.vm.tfsaData.afterTaxFutureValue.toFixed(2)).toBe("3153.35")
  })

  it('should calculate RRSP', () => {
    // setup test values
    wrapper.vm.userData.marginalTaxRate = 38.29
    wrapper.vm.userData.retirementTaxRate = 20.64
    wrapper.vm.userData.deposit = 1000
    wrapper.vm.userData.years = 30
    wrapper.vm.userData.roi = 6.5
    wrapper.vm.userData.inflation = 2.5

    // calculate
    button.trigger('click')
    expect(wrapper.vm.rrspData.totalDeposit.toFixed(2)).toBe("1620.48")
    expect(wrapper.vm.rrspData.futureValue.toFixed(2)).toBe("5109.95")
    expect(wrapper.vm.rrspData.futureTaxPaid.toFixed(2)).toBe("-1054.69")
    expect(wrapper.vm.rrspData.afterTaxFutureValue.toFixed(2)).toBe("4055.26")
  })

  it('should calculate future value', () => {
    expect(wrapper.vm.futureValue(1000, 1.5, 12).toFixed(2)).toBe("1195.62")
    expect(wrapper.vm.futureValue(10000, 3.5, 48).toFixed(2)).toBe("52135.89")
    expect(wrapper.vm.futureValue(5000, 4, 12).toFixed(2)).toBe("8005.16")
    expect(wrapper.vm.futureValue(5000, 2.321, 84).toFixed(2)).toBe("34356.68")
  })

  it('should calculate real rate of return', () => {
    expect(wrapper.vm.realRateOfReturn(5, 2).toFixed(3)).toBe("2.941")
    expect(wrapper.vm.realRateOfReturn(6.5, 2.5).toFixed(3)).toBe("3.902")
    expect(wrapper.vm.realRateOfReturn(9.111, 1.75).toFixed(3)).toBe("7.234")
    expect(wrapper.vm.realRateOfReturn(36.123, 37.125).toFixed(3)).toBe("-0.731")
  })
})
